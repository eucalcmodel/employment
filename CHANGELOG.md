# Changelog

## Unreleased
### Added
- Input data files
- Interactions with all the other modules
- Nodes to create indicator of transitions 
- Nodes to compute employment impacts

### Changes
- 

### Removes
- 

### Fixed 
- 

## 1.0
- Initial version, integrating all modules